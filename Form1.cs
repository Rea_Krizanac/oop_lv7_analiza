using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace kružić_križić
{
    public partial class Form1 : Form
    {
        bool na_redu = true; // igrač koji je na redu 
        int brojac = 0;
        static String igrac1, igrac2;

        public Form1()
        {

            InitializeComponent();
        }

        public static void Postavi_imena_igraca(string a, string b)
        {
            igrac1 = a;
            igrac2 = b;
        }

        private void btn_izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_nova_igra_Click(object sender, EventArgs e)
        {
            na_redu = true;
            brojac = 0;
            label4.Text = igrac1;

            A1.Enabled = true;
            A1.Text = "";
            A2.Enabled = true;
            A2.Text = "";
            A3.Enabled = true;
            A3.Text = "";
            B1.Enabled = true;
            B1.Text = "";
            B2.Enabled = true;
            B2.Text = "";
            B3.Enabled = true;
            B3.Text = "";
            C1.Enabled = true;
            C1.Text = "";
            C2.Enabled = true;
            C2.Text = "";
            C3.Enabled = true;
            C3.Text = "";

            text1_ime.Clear();
            text2_ime.Clear();

        }

        private void button_click(object sender, EventArgs e) // kućice
        {
            Button b = (Button)sender;
            if (na_redu)
            {
                b.Text = "X";
                label4.Text = igrac2;
            }
            else
            {
                b.Text = "O";
                label4.Text = igrac1;
            }

            na_redu = !na_redu;
            b.Enabled = false;
            brojac++;
            Provjeri_tko_je_pobijedio();
        }

        public void Provjeri_tko_je_pobijedio()
        {

            bool Pobijedio_si = false;
            if ((A1.Text == A2.Text) && (A2.Text == A3.Text) && (!A1.Enabled)) // provjerava horizontalno
                Pobijedio_si = true;
            if ((B1.Text == B2.Text) && (B2.Text == B3.Text) && (!B1.Enabled)) // provjerava horizontalno
                Pobijedio_si = true;
            if ((C1.Text == C2.Text) && (C2.Text == C3.Text) && (!C1.Enabled)) // provjera horizontalno

                Pobijedio_si = true;

            else if ((A1.Text == B1.Text) && (B1.Text == C1.Text) && (!A1.Enabled)) // provjerava okomito
                Pobijedio_si = true;
            else if ((A2.Text == B2.Text) && (B2.Text == C2.Text) && (!A2.Enabled)) // provjerava okomito
                Pobijedio_si = true;
            else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (!A3.Enabled)) // provjera okomito

                Pobijedio_si = true;

            else if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (!A1.Enabled)) // provjerava dijagonalno
                Pobijedio_si = true;
            else if ((A3.Text == B2.Text) && (B2.Text == C1.Text) && (!C1.Enabled)) // provjerava dijagonalno

                Pobijedio_si = true;


            if (Pobijedio_si)
            {
                onesposobi_buttons();
                String pobjednik = "";
                if (na_redu)
                {
                    pobjednik = igrac2;
                    label8.Text = (Int32.Parse(label8.Text) + 1).ToString();


                }
                else
                {
                    pobjednik = igrac1;
                    label7.Text = (Int32.Parse(label7.Text) + 1).ToString();


                }

                MessageBox.Show(pobjednik + " je pobijedio / la!!");

            }
            else if (brojac == 9)
            {

                MessageBox.Show("Neriješeno!!");
            }

        }

        private void onesposobi_buttons()
        {
            try
            {
                foreach (Control c in Controls)
                {

                    Button b = (Button)c;
                    b.Enabled = false;

                }
            }
            catch { }


        }

        private void pokreni_igru_Click(object sender, EventArgs e)
        {
            Form1.Postavi_imena_igraca(text1_ime.Text, text2_ime.Text);
            label5.Text = igrac1;
            label6.Text = igrac2;
            label4.Text = igrac1;
        }
    }
}
